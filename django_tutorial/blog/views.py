from django.shortcuts import render
from django.http import HttpResponse
from .models import Post

def home(request):
    """Returns what users want to see when
    they are sent to this route"""
    context = {
        'posts': Post.objects.all()
    }
    return render(request, "blog/home.html", context)

def about(request):
    return render(request, "blog/about.html", context={'title':'About'})